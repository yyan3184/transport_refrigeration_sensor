# Transport Refrigerator Sensor

This project creates the temprerature monitor/sensor system aims to help Baz, the brewery truck driver, show him the real-time temperature of each single beer container in a readable visualized table.

It also allows the driver to remotely control the temperature by re-setting containers back to normal temperature. (Let's assume that the embedded control technology and network control technology is already implemented and applied through the process).

This project is built up from scratch by ReactJs & Redux, with all the mock data setup in the application layer.

Since we have the temperature ranges for each kind of beer, we may also need 5 large containers to store each kind of beer properly with a suitable initial temperature.

The initial temperature of each container is set as the median number of each beer's temperature range:

* Container 1, stores beer 1 -> initial temperature: 5°C    -> temperature range (4°C - 6°C)
* Container 2, stores beer 2 -> initial temperature: 5.5°C  -> temperature range (5°C - 6°C)
* Container 3, stores beer 3 -> initial temperature: 5.5°C  -> temperature range (4°C - 7°C)
* Container 4, stores beer 4 -> initial temperature: 7°C    -> temperature range (6°C - 8°C)
* Container 5, stores beer 5 -> initial temperature: 4°C    -> temperature range (3°C - 5°C)

The reasons that cause the container's temperature fall out of range can be various. In order to test the effeciveness of the program, every 5 seconds, the system would randomly select a container, follow by allocating it a random temperature value which fluactuates around the temperature range.

Also, there are 3 signal lights act as indicator to show the driver what the temperature status is.

* Green, means the status is SAFE, the container's temperature is normal
* Yellow, means the status is CAUTIOUS, the container's temperature is about to fall out of range, normally less than 0.3°C to the boundary
* Red, means the status is DANGEROUS, the container's temperature is already out of range and needs to be re-set straight away

## Getting Started

These instructions below will get you a copy of the project up and running on your local machine for development and testing purposes.

From a terminal window, change to the local directory where you want to clone your repository.
Paste the command you copied from Bitbucket
```
 $ git clone https://yyan3184@bitbucket.org/yyan3184/transport_refrigeration_sensor.git
```
If the clone was successful, a new directory appears on your local drive. The directory is named 'transport_refrigeration_sensor', change directory into directory '/transport_refrigeration_sensor'.   


### Installing

Run the command to install all the local npm packages and dependencies.
```
 $ npm install
```

Once the installation completed, you will see a new folder 'node_modules' created under the directory.

### Running

Run the command to start the program.
```
 $ npm start
```
Open [http://localhost:3000](http://localhost:3000) to view it in the browser. You will see the refrigerator sensor systems which is monitorning all the 5 containers with randomly changed temperature on random container every 5 seconds.

The signal light indicator changes when the corresponding container's current temperature fluctuates, meanwhile the driver is able to click the RESET button to fix it when it goes to RED.


## Running the tests

Let's also run the testing process, it provides really helpful validation to the code's robustness, component rendering & redux state.
```
 $ npm test
```
The test is successful when all the 8 tests passed, with different parts validating the rendering, the signal light returned by different range & container, the returned redux state data, and the re-set button mock click function got called.

```
PASS  src/App.test.js
  Test Rendering
    ✓ render without crashing (62ms)
  Test Temperature Status Color
    ✓ should return GREEN color when temperature is in the range & greater than 0.3°C to the max / min (4ms)
    ✓ should return YELLOW color when temperature is in the range & less than 0.3°C to the max / min (1ms)
    ✓ should return RED color when temperature is out of the range (1ms)
  Test Redux Reducer
    ✓ should return the initial state (1ms)
    ✓ should handle ACCIDENTALLY_CHANGE_TEMPERATURES, randomly change one of the container's temperature, while other containers' temperature are not affected (3ms)
    ✓ should handle RESET_TEMPERATURES, reset all containers' temperature back to initial default state (1ms)
  Test Button Component
    ✓ click the reset temperature button has been called (4ms)

Test Suites: 1 passed, 1 total
Tests:       8 passed, 8 total
Snapshots:   0 total
Time:        4.078s
Ran all test suites.
```

## Logic / Coding Style

The whole application's state is quite simple, 5 containers with each one's current temperature and signal color. I created the reducer to specify how the containers' temperature changes in response to random temperature fluctuation sent to the store.

The actions dispatched include "Randomly select a container to give the temperature fluctuation, with the corresponding container number and new signal color" and "driver click the re-set temperature button to fix it."

```
export const randomlyChangeTemperature = (container, temperature, color) => ({
    type: 'ACCIDENTALLY_CHANGE_TEMPERATURES',
    container,
    temperature,
    color
});

export const resetTemperature = (container) => ({
    type: 'RESET_TEMPERATURES'
})
```


Reducer "currentTemperatures" will handle above 2 actions with previous state, and return the next state to indicate all containers' tempereature and signal colors.

```
const initialState = [
    {value: 5, color: COLOR.GREEN},
    {value: 5.5, color: COLOR.GREEN},
    {value: 5.5, color: COLOR.GREEN},
    {value: 7, color: COLOR.GREEN},
    {value: 4, color: COLOR.GREEN}];

const currentTemperatures = (state = initialState, action) => {
    switch (action.type) {
        case ACCIDENTALLY_CHANGE_TEMPERATURES:
            return [
                ...state.slice(0, action.container - 1),
                {value: action.temperature, color: action.color},
                ...state.slice(action.container)
            ];
        case RESET_TEMPERATURES:
            return initialState;
        default:
            return state;
    }
}
```

This function is used for analyzing the current temperature and the container's beer temperature ranges, and return the most suitable signal color
```
//return the color for temperature indicator status
function _returnStatusColor(temperature, beerTempRanges) {
    const min = beerTempRanges.min, max = beerTempRanges.max;

    //if the temperature is in the range
    if (temperature >= min && temperature <= max) {
        //if the temperature is not close to the max / min, let's suppose the difference is greater than 0.3°C
        if (((temperature - min) > 0.3) && ((max - temperature) > 0.3)) {
            return COLOR.GREEN;
        } else {
            return COLOR.YELLOW;
        }
    } else {
        //if the temperature is out of range
        return COLOR.RED;
    }
}
```

In order to continuously mock the fluctuated temperature to different container, I set up a interval function calling at every 5 seconds to fluactuate one container's temperature.

```
this.tempInterval = setInterval(() => {
            const containerNo = _generateRandomInt(1, 5);
            const temperature = _generateRandomTemperature(beerTempRanges[containerNo - 1].min, beerTempRanges[containerNo - 1].max);
            const color = _returnStatusColor(temperature, beerTempRanges[containerNo - 1]);
            randomlyChangeTemperature(containerNo, temperature, color);
        }, 5000);
```

this will trigger the action to be dispatched at the end of function. The inteval ID value will be cleard when clearInterval() called when window closed.

![architecture](https://www.myagentsprofile.com.au/Agents_Profile/user_photo/architecture_2018_03_28.jpg)



## Improvement

* The system will be more readable if there are timer counter displays next to the signal light indicator, showing driver home long has passed since the light turned red.
* The reasons why the temperature falls out of range and seggested solution can be estimated by the fluctuation rate, while it needs more work and time to design that.    

## Built With

* [Reactjs](https://reactjs.org/) - The Javascript library used
* [Redux](https://redux.js.org/introduction) - The JavaScript library designed for managing application state
* [JEST](https://facebook.github.io/jest/) - JavaScript Testing Tool



