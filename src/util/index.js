/**
 * Created by Yubo on 2018/3/27.
 */

const COLOR = {YELLOW: 'yellow', RED: 'red', GREEN: 'green'};

//randomly generate the index number of container
function _generateRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}


//randomly generate one particular container's current temperature
function _generateRandomTemperature(min, max) {
    return Math.floor(10 * (Math.random() * (max - min + 1) + min)) / 10;
}

//return the color for temperature indicator status
function _returnStatusColor(temperature, beerTempRanges) {
    const min = beerTempRanges.min, max = beerTempRanges.max;

    //if the temperature is in the range
    if (temperature >= min && temperature <= max) {
        //if the temperature is not close to the max / min, let's suppose the difference is greater than 0.3°C
        if (((temperature - min) > 0.3) && ((max - temperature) > 0.3)) {
            return COLOR.GREEN;
        } else {
            return COLOR.YELLOW;
        }
    } else {
        //if the temperature is out of range
        return COLOR.RED;
    }
}

//set up initial default temperature for each container & the temperature range of each kind of beer
const sensorsInitialState = {
    defaultTemperatures: [5, 5.5, 5.5, 7, 4],
    beerTempRanges: [
        {min: 4, max: 6},
        {min: 5, max: 6},
        {min: 4, max: 7},
        {min: 6, max: 8},
        {min: 3, max: 5}
    ]
}


export {_generateRandomInt, _generateRandomTemperature, _returnStatusColor, sensorsInitialState, COLOR};