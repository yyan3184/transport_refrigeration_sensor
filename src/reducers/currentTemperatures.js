/**
 * Created by Yubo on 2018/3/25.
 */

import {COLOR} from '../util';
const ACCIDENTALLY_CHANGE_TEMPERATURES = 'ACCIDENTALLY_CHANGE_TEMPERATURES';
const RESET_TEMPERATURES = 'RESET_TEMPERATURES';


const initialState = [
    {value: 5, color: COLOR.GREEN},
    {value: 5.5, color: COLOR.GREEN},
    {value: 5.5, color: COLOR.GREEN},
    {value: 7, color: COLOR.GREEN},
    {value: 4, color: COLOR.GREEN}];

const currentTemperatures = (state = initialState, action) => {
    switch (action.type) {
        case ACCIDENTALLY_CHANGE_TEMPERATURES:
            return [
                ...state.slice(0, action.container - 1),
                {value: action.temperature, color: action.color},
                ...state.slice(action.container)
            ];
        case RESET_TEMPERATURES:
            return initialState;
        default:
            return state;
    }
}

export default currentTemperatures;