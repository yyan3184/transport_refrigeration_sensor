/**
 * Created by Yubo on 2018/3/25.
 */
import {combineReducers} from 'redux';
import currentTemperatures from './currentTemperatures';


const rootReducer = combineReducers({
    currentTemperatures
});



export default rootReducer;