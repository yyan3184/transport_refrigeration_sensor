/**
 * Created by Yubo on 2018/3/25.
 */

export const randomlyChangeTemperature = (container, temperature, color) => ({
    type: 'ACCIDENTALLY_CHANGE_TEMPERATURES',
    container,
    temperature,
    color
});

export const resetTemperature = (container) => ({
    type: 'RESET_TEMPERATURES'
})