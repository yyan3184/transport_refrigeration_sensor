import React from 'react';
import ReactDOM from 'react-dom';
import {shallow, configure} from 'enzyme';
import configureStore from './configureStore';
import {Provider} from 'react-redux';
import Adapter from 'enzyme-adapter-react-16';
import App from './App';
import ResetButton from './components/ResetButton';
import currentTemperatures from './reducers/currentTemperatures';
import {_generateRandomInt, _generateRandomTemperature, _returnStatusColor} from './util';
import {COLOR} from './util';

const store = configureStore();

const initialState = [{value: 5, color: COLOR.GREEN}, {value: 5.5, color: COLOR.GREEN}, {value: 5.5, color: COLOR.GREEN}, {value: 7, color: COLOR.GREEN}, {value: 4, color: COLOR.GREEN}];


describe('Test Rendering', () => {
    it('render without crashing', () => {
        const div = document.createElement('div');
        ReactDOM.render(
            <Provider store={store}>
                <App />
            </Provider>, div);
        ReactDOM.unmountComponentAtNode(div);
    });
});

describe('Test Temperature Status Color', () => {
    const beerTempRange = {min: 5, max: 6};
    it('should return GREEN color when temperature is in the range & greater than 0.3°C to the max / min', () => {
        expect(_returnStatusColor(5.5, beerTempRange)).toEqual(COLOR.GREEN);
    });
    it('should return YELLOW color when temperature is in the range & less than 0.3°C to the max / min', () => {
        expect(_returnStatusColor(5.9, beerTempRange)).toEqual(COLOR.YELLOW);
    });
    it('should return RED color when temperature is out of the range', () => {
        expect(_returnStatusColor(6.1, beerTempRange)).toEqual(COLOR.RED);
    });
})


describe('Test Redux Reducer', () => {
    it('should return the initial state', () => {
        expect(currentTemperatures(undefined, {})).toEqual(initialState);
    });

    it('should handle ACCIDENTALLY_CHANGE_TEMPERATURES, randomly change one of the container\'s temperature, ' +
        'while other containers\' temperature are not affected', () => {
        const container = _generateRandomInt(1, 5);
        const temperature = _generateRandomTemperature(5, 7);
        const changeAction = {
            type: 'ACCIDENTALLY_CHANGE_TEMPERATURES',
            container,
            temperature
        };
        expect(currentTemperatures(undefined, changeAction)[container-1].value).toEqual(temperature);
        expect(currentTemperatures(undefined, changeAction)).toHaveLength(initialState.length);
    });

    it('should handle RESET_TEMPERATURES, reset all containers\' temperature back to initial default state', () => {
        const resetAction = {
            type: 'RESET_TEMPERATURES'

        };
        expect(currentTemperatures(undefined, resetAction)).toEqual(initialState);
    });
});

describe('Test Button Component', () => {
    it('click the reset temperature button has been called', () => {
        const mockCallBack = jest.fn();
        configure({adapter: new Adapter()});
        const tree = shallow(
            <ResetButton onClick={mockCallBack}/>
        );
        tree.simulate('click');
        expect(mockCallBack).toHaveBeenCalled();
    });
});