/**
 * Created by Yubo on 2018/3/25.
 */
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware, compose } from 'redux';
import * as thunk from 'redux-thunk';
import appStateReducers from './reducers';

const configureStore = () =>{
    const middlewares = [thunk];
    if(process.env.NODE_ENV !== 'production') {
        middlewares.push(createLogger());
    }
    return createStore(
        appStateReducers,
        compose(
            applyMiddleware(thunk.default),
            window.devToolsExtension ? window.devToolsExtension() : f => f
        )
    );
};

export default configureStore;