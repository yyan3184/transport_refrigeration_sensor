import React, {Component} from 'react';
import {connect} from 'react-redux';
import {randomlyChangeTemperature, resetTemperature} from './actions';
import {sensorsInitialState, _generateRandomInt, _generateRandomTemperature, _returnStatusColor} from './util';
import ResetButton from './components/ResetButton';
import TemperatureIndicator from './components/TemperatureIndicator';
import {COLOR} from './util';


export class App extends Component {
    constructor() {
        super();
        this.state = sensorsInitialState;
    }

    componentDidMount() {
        const {beerTempRanges} = this.state;
        const {randomlyChangeTemperature} = this.props;
        this.tempInterval = setInterval(() => {
            const containerNo = _generateRandomInt(1, 5);
            const temperature = _generateRandomTemperature(beerTempRanges[containerNo - 1].min, beerTempRanges[containerNo - 1].max);
            const color = _returnStatusColor(temperature, beerTempRanges[containerNo - 1]);
            randomlyChangeTemperature(containerNo, temperature, color);
        }, 5000);
    }

    componentWillUnmount() {
        clearInterval(this.tempInterval);
    }


    _displayTemperatures = (currentTemperatures) => {
        const {defaultTemperatures, beerTempRanges} = this.state;
        return currentTemperatures.map((containerTemperature, i) => {
            return (
                <tr key={i + 1}>
                    <td>{i + 1}</td>
                    <td>
                        <TemperatureIndicator temperature={containerTemperature} beerTempRanges={beerTempRanges[i]}/>
                    </td>
                    <td>
                        <span style={{fontWeight: 'bold'}}>{containerTemperature.value}</span>
                    </td>
                    <td>{defaultTemperatures[i]} ({beerTempRanges[i].min} - {beerTempRanges[i].max})</td>
                    <td>{i + 1}</td>
                </tr>);
        });
    }

    _resetTemperature = () => {
        this.props.resetTemperature();
    }

    render() {
        const {currentTemperatures} = this.props;
        return (
            <div className="App">
                <div className="container">
                    <div className="row">
                        <h1>Transport Refrigerator Sensor</h1>
                        <div className="color-instruction">
                            <div className="color-indicator" style={{backgroundColor: COLOR.GREEN}}/>
                            <span>safe</span>
                            <div className="color-indicator" style={{backgroundColor: COLOR.YELLOW}}/>
                            <span>caution</span>
                            <div className="color-indicator" style={{backgroundColor: COLOR.RED}}/>
                            <span>danger</span>
                        </div>
                        <div className="col-xs-12 table">
                            <div className="table-responsive">
                                <table className="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>Container</th>
                                        <th>Status</th>
                                        <th>Current Temperature</th>
                                        <th>Default Temperature</th>
                                        <th>Beer</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {this._displayTemperatures(currentTemperatures)}
                                    </tbody>
                                </table>
                                <ResetButton onClick={this._resetTemperature} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, onwProps) => {
    return {
        currentTemperatures: state.currentTemperatures
    }
}

export default connect(mapStateToProps, {randomlyChangeTemperature, resetTemperature})(App);

