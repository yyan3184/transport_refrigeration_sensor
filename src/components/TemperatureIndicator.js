/**
 * Created by Yubo on 2018/3/26.
 */
import React, {Component} from 'react';

export default class TemperatureIndicator extends Component {

    render() {
        const {temperature} = this.props;
        return (
            <div>
                <div className="color-indicator" style={{backgroundColor: temperature.color}}/>
            </div>
        );
    }
}