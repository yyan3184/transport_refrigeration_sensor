/**
 * Created by Yubo on 2018/3/27.
 */
import React from 'react';

const ResetButton = (props) => {
    return (
        <button className="reset-btn" onClick={props.onClick}>Reset</button>
    );
}

export default ResetButton;